﻿using UnityEngine;
using System.Collections;

namespace TestEffect
{
	public static class Constants 
	{
		static public float ANIMATION_SPEED = 2.5f;
		static public float PLUME_ANIMATION_SPEED = 7f;
		static public float WAVE_AMPLITUDE = 0.5f;
		static public float ROTATION_SPEED = 0.2f;
	}
}