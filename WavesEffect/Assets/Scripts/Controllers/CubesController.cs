﻿using UnityEngine;
using System.Collections;

namespace TestEffect
{
	public class CubesController : Controller 
	{
		public CubeElement elementPrefab;
		public Color[] colors;
		public Material materialPrefab;
		Material[] materials = new Material[5];

		protected override void SetupMaterials()
		{
			CreateMaterials();
			AssignMaterials();
		}

		void CreateMaterials()
		{
			for(int i = 0; i < 5; ++i)
			{
				materials[i] = Instantiate<Material>(materialPrefab);
				materials[i].color = colors[i];
			}
		}

		void AssignMaterials()
		{
			for(int i = width / -2; i <= width / 2; ++i)	
			{
				for(int j = height / -2; j <= height / 2; ++j)
				{
					elements[new Vector2(i, j)].GetComponent<MeshRenderer>().material = materials[Random.Range(0, 5)];
				}
			}
		}

		protected override Element CreateElement()
		{
			Element result = Instantiate<CubeElement>(elementPrefab);
			result.transform.SetParent(transform);
			result.transform.localScale = new Vector3(1f, 10f, 1f);

			return result;
		}
	}
}