﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

namespace TestEffect
{
	public class UIController : MonoBehaviour 
	{
		public Slider attenuatuon;
		public Slider plume;

		void Start()
		{
			UpdateValues();
		}

		void UpdateValues()
		{
			Controller.Instance.attenuation = attenuatuon.value;
			Controller.Instance.plume = plume.value;
		}

		public void OnValueChange()
		{
			UpdateValues();
		}

		public void Exit()
		{
			Application.LoadLevel("Menu");
//			UnityEngine.SceneManagement.SceneManager.LoadScene("Menu");
		}
	}
}