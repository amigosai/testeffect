﻿using UnityEngine;
using System.Collections;

namespace TestEffect
{
	public class Element : MonoBehaviour 
	{
		public float range;
		public float speed;
		public Vector2 index;
		public float destination;

		float waveOffset;

		void Update()
		{
			if(!Controller.Instance.activeMode)
				NonActiveState();
			else
				ActiveState();
		}

		void ActiveState()
		{
			waveOffset = Constants.WAVE_AMPLITUDE * Mathf.Sin( Vector2.Distance(Controller.Instance.effectCenter, index) * Controller.Instance.waveTime - (Time.time - Controller.Instance.startTime) * Constants.ANIMATION_SPEED );

			if(( Vector2.Distance(Controller.Instance.effectCenter, index) > (1f - Controller.Instance.waveTime) * Controller.Instance.maxRadius ) )// ||
				waveOffset = 0f;

			if(Vector2.Distance(Controller.Instance.effectCenter, index) < 3f)
				waveOffset *= 0.3f;

			transform.position = new Vector3(transform.position.x,
				Mathf.Lerp(transform.position.y, Controller.Instance.plume / (Vector2.Distance(Controller.Instance.effectCenter, index) + 1f), Time.deltaTime * Constants.PLUME_ANIMATION_SPEED)
			                                 + waveOffset * Controller.Instance.waveTime,
			                                 transform.position.z);
		}

		protected virtual void NonActiveState()
		{
			transform.position = new Vector3(transform.position.x,
				Mathf.Lerp(transform.position.y, Mathf.PingPong(Time.time * speed, range), Time.deltaTime * Constants.ANIMATION_SPEED),
			                                 transform.position.z);
		}

	}
}
