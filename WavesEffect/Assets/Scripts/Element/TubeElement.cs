﻿using UnityEngine;
using System.Collections;

namespace TestEffect
{
	public class TubeElement : Element
	{
		protected override void NonActiveState()
		{
			transform.position = new Vector3(transform.position.x,
				Mathf.Lerp(transform.position.y, Mathf.Sin(index.magnitude - Time.time * Constants.ANIMATION_SPEED), Time.deltaTime * Constants.ANIMATION_SPEED),
			                                 transform.position.z);
		}
	}
}
