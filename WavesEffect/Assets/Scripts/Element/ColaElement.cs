﻿using UnityEngine;
using System.Collections;

namespace TestEffect
{
	public class ColaElement : Element
	{
		const float ROTATION_RANGE = 10f;

		protected override void NonActiveState()
		{
			base.NonActiveState();

			transform.eulerAngles = new Vector3(transform.eulerAngles.x,
												Mathf.PingPong(Time.time * speed * Constants.ANIMATION_SPEED, 2f * ROTATION_RANGE) - ROTATION_RANGE,
			                                    transform.eulerAngles.z);
		}
	}
}