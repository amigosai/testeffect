﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace TestEffect
{
	public class Controller : MonoBehaviour 
	{
		public static Controller Instance { get; private set; }

		public bool activeMode;
		public float attenuation;
		public float plume;
		public float waveTime;
		public float waveUpdateTime;
		public float maxRadius;
		public float startTime;
		public Vector2 effectCenter;

		protected int width;
		protected int height;
		protected Dictionary<Vector2, Element> elements = new Dictionary<Vector2, Element>();

		const float MIN_SPEED = 2f;
		const float MAX_SPEED = 35f;
		const float MIN_RANGE = 15f;
		const float MAX_RANGE = 30f;
		const float PLUME_DISTANCE = 5f;

		Vector2 previousCenter;

		void Awake()
		{
			Instance = this;
			width = DataTransfering.width;
			height = DataTransfering.height;

			//temp
			if(width == 0)
				width = 32;
			if(height == 0)
				height = 24;

			maxRadius = new Vector2(width, height).magnitude;
			CreateElements();
			SetupMaterials();
		}

		protected virtual void SetupMaterials()
		{
			//stub
		}

		void CreateElements()
		{
			for(int i = width / -2; i <= width / 2; ++i)	
			{
				for(int j = height / -2; j <= height / 2; ++j)
				{
					Element element = CreateElement();

					if(element == null)
						return;

					element.transform.position = new Vector3(i, 0f, j);

					element.speed = Random.Range(MIN_SPEED, MAX_SPEED) * 0.1f;
					element.range = Random.Range(MIN_RANGE, MAX_RANGE) * 0.1f;

					element.index = new Vector2(i, j);
					elements.Add(element.index, element);
				}
			}
			effectCenter = Vector2.zero;
		}

		protected virtual Element CreateElement()
		{
			return null;
		}

		void Update()
		{
			if(Input.GetMouseButton(0))
			{
				effectCenter = ElementUnderTouch( Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, Camera.main.transform.position.y * 0.8f)) );
				if(previousCenter != effectCenter || activeMode == false)
				{
					previousCenter = effectCenter;
					startTime = Time.time;
					waveUpdateTime = attenuation;
				}

				if(waveUpdateTime >= 0f)
				{
					waveUpdateTime -= Time.deltaTime;
					waveTime = waveUpdateTime / attenuation;
				}

				activeMode = true;
				return;
			}
			activeMode = false;
		}

		Vector2 ElementUnderTouch(Vector3 position)
		{
			return new Vector2((int)position.x, (int)position.z);
		}

	}
}