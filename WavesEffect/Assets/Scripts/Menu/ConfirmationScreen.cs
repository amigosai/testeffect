﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

namespace TestEffect
{
	public enum TaskType
	{
		Cubes = 0,
		Tubes,
		Cola,
	}

	public class ConfirmationScreen : MonoBehaviour 
	{
		public Text width;
		public Slider widthSlider;

		public Text height;
		public Slider heightSlider;

		TaskType type;

		public Vector2[] widthRange;
		public Vector2[] heightRange;

		public Vector2[] defaultSize;

		string[] levelNames = {"Cubes", "Tubes", "Cola"};

		public void ShowScreen(TaskType type)
		{
			this.type = type;

			widthSlider.minValue = widthRange[(int)type].x;
			widthSlider.maxValue = widthRange[(int)type].y;

			heightSlider.minValue = heightRange[(int)type].x;
			heightSlider.maxValue = heightRange[(int)type].y;

			widthSlider.value = defaultSize[(int)type].x;
			heightSlider.value = defaultSize[(int)type].y;

			UpdateWithSize(defaultSize[(int)type]);
			gameObject.SetActive(true);
		}

		public void OnValueChanged()
		{
			UpdateWithSize(new Vector2(widthSlider.value, heightSlider.value));
		}

		void UpdateWithSize(Vector2 newSize)
		{
			widthSlider.value = newSize.x;
			heightSlider.value = newSize.y;
			width.text = "Width: " + newSize.x.ToString();
			height.text = "Height: " + newSize.y.ToString();
		}

		public void Confirm()
		{
			DataTransfering.width = (int)widthSlider.value;
			DataTransfering.height = (int)heightSlider.value;
			Application.LoadLevel(levelNames[(int)type]);
//			UnityEngine.SceneManagement.SceneManager.LoadScene(levelNames[(int)type]);
		}
	}
}