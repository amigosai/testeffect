﻿using UnityEngine;
using System.Collections;

namespace TestEffect
{
	public class ColaController : Controller 
	{
		public ColaElement elementPrefab;

		protected override Element CreateElement()
		{
			Element result = Instantiate<ColaElement>(elementPrefab);
			result.transform.SetParent(transform);
			result.transform.localScale = new Vector3(1f, 0.5f, 1f);

			return result;
		}
	}
}