﻿using UnityEngine;
using System.Collections;

namespace TestEffect
{
	public class TubesController : Controller 
	{
		public TubeElement elementPrefab;

		protected override Element CreateElement()
		{
			Element result = Instantiate<TubeElement>(elementPrefab);
			result.transform.SetParent(transform);
			result.transform.localScale = new Vector3(1f, 2f, 1f);
			
			return result;
		}
	}
}