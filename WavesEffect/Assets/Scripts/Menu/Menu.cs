﻿using UnityEngine;
using System.Collections;

namespace TestEffect
{
	public class Menu : MonoBehaviour 
	{
		public ConfirmationScreen confirmation;

		public void Cubes()
		{
			confirmation.ShowScreen(TaskType.Cubes);
		}

		public void Tubes()
		{
			confirmation.ShowScreen(TaskType.Tubes);
		}

		public void Cola()
		{
			confirmation.ShowScreen(TaskType.Cola);
		}
	}
}